package me.gianfebrian.smshooker;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ConfigSQLiteDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "sms_reader";
    public static final String CONFIG_TABLE_NAME = "config";
    public static final String CONFIG_COLUMN_ID = "_id";
    public static final String CONFIG_COLUMN_URI = "uri";
    public static final String CONFIG_COLUMN_SERVICE_NUMBER = "service_number";
    public static final String CONFIG_COLUMN_SERVICE_CONDITION = "service_condition";


    public ConfigSQLiteDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqliteDatabase) {
        try {
            sqliteDatabase.execSQL("CREATE TABLE " + CONFIG_TABLE_NAME + "( " +
                    CONFIG_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    CONFIG_COLUMN_URI + " TEXT, " +
                    CONFIG_COLUMN_SERVICE_NUMBER + " TEXT, " +
                    CONFIG_COLUMN_SERVICE_CONDITION + " TEXT" + " )"
            );
        } catch (Exception e) {
            Log.e("SQLite onCreate", e.toString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqliteDatabase, int i, int i1) {
        try {
            sqliteDatabase.execSQL("DROP TABLE IF EXISTS " + CONFIG_TABLE_NAME);
        } catch (Exception e) {
            Log.e("SQLite onUpgrade", e.toString());
        }

        onCreate(sqliteDatabase);
    }

}
