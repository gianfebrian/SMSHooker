package me.gianfebrian.smshooker;

import android.Manifest;
import android.content.ContentValues;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private SmsBroadcastReceiver smsBroadcastReceiver;

    private RequestQueue rQueue;

    private EditText textWebhookURI;
    private EditText textServiceNumber;
    private EditText textServiceCondition;

    private TextView textResponseLog;

    private Button buttonSave;
    private Button buttonTestHook;
    private Button buttonControl;

    private EditText textSMSContent;

    private int SMS_PERMISSION_CODE, INTERNET_PERMISSION_CODE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textWebhookURI = (EditText) findViewById(R.id.text_webhook_uri);
        textServiceNumber = (EditText) findViewById(R.id.text_service_number);
        textServiceCondition = (EditText) findViewById(R.id.text_service_condition);

        textResponseLog = (TextView) findViewById(R.id.text_response_log);

        buttonSave = (Button) findViewById(R.id.button_save);
        buttonControl = (Button) findViewById(R.id.button_control);
        buttonTestHook = (Button) findViewById(R.id.button_test_hook);

        textSMSContent = (EditText) findViewById((R.id.multi_sms_content));

        if (!isSmsPermissionGranted()) {
            requestReadAndSendSmsPermission();
        }

        if (!isInternetPermissionGranted()) {
            requestInternetPermission();
        }

        readFromDB();

        rQueue = Volley.newRequestQueue(this);

        smsBroadcastReceiver = new SmsBroadcastReceiver(
                textServiceNumber.getText().toString(),
                textServiceCondition.getText().toString()
        );

        registerReceiver(
                smsBroadcastReceiver,
                new IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)
        );

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToDB();
            }
        });


        buttonControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSMSToWebHook(
                        textSMSContent.getText().toString(),
                        Request.Method.POST,
                        textWebhookURI.getText().toString()
                );
            }
        });

        buttonTestHook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                testWebHook(Request.Method.POST, textWebhookURI.getText().toString());
            }
        });

        smsBroadcastReceiver.setListener(new SmsBroadcastReceiver.Listener() {
            @Override
            public void onTextReceived(String text) {
                textSMSContent.setText(text);
                sendSMSToWebHook(text, Request.Method.POST, textWebhookURI.getText().toString());
            }
        });
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(smsBroadcastReceiver);
        super.onDestroy();
    }

    @Override
    protected void onStop () {
        super.onStop();
        if (rQueue != null) {
            rQueue.cancelAll("SmsReader");
        }
    }

    public boolean isSmsPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestReadAndSendSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)) {
            // You may display a non-blocking explanation here, read more in the documentation:
            // https://developer.android.com/training/permissions/requesting.html
        }

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, SMS_PERMISSION_CODE);
    }

    public boolean isInternetPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestInternetPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)) {
            // You may display a non-blocking explanation here, read more in the documentation:
            // https://developer.android.com/training/permissions/requesting.html
        }

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, INTERNET_PERMISSION_CODE);
    }

    private Map<String, String> parseSMS(String content, String template) {
        Map<String, String> out = new HashMap<String, String>();

        String[] input = content.split(" ");
        String[] parserTemplate = template.split(" ");

        for (int i = 0; i < parserTemplate.length; i++) {
            if (parserTemplate[i].contains("{{") && parserTemplate[i].contains("}}")) {
                String key = parserTemplate[i].replace("{{", "");
                key = key.replace("}}", "");
                out.put(key, input[i]);
            }
        }


        return out;
    }

    private void sendSMSToWebHook(String content, int method, String url) {
        Map<String, String> payload = parseSMS(
                content,
                textServiceCondition.getText().toString()
        );

        payload.put("fulltext", content);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                method,
                url,
                new JSONObject(payload),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        textResponseLog.setText(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        textResponseLog.setText(error.toString());
                    }
                }
        );

        rQueue.add(jsonRequest);
    }


    private void testWebHook(int method, String url) {
        JsonObjectRequest stringRequest = new JsonObjectRequest(
                method,
                url,
                new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        textResponseLog.setText(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        textResponseLog.setText(error.toString());
                    }
                }
        );

        rQueue.add(stringRequest);
    }

    private void saveToDB() {
        SQLiteDatabase database = new ConfigSQLiteDBHelper(this).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ConfigSQLiteDBHelper.CONFIG_COLUMN_ID, 1);
        values.put(ConfigSQLiteDBHelper.CONFIG_COLUMN_URI, textWebhookURI.getText().toString());
        values.put(ConfigSQLiteDBHelper.CONFIG_COLUMN_SERVICE_NUMBER, textServiceNumber.getText().toString());
        values.put(ConfigSQLiteDBHelper.CONFIG_COLUMN_SERVICE_CONDITION, textServiceCondition.getText().toString());

        int rows = database.update(
                ConfigSQLiteDBHelper.CONFIG_TABLE_NAME,
                values,
                ConfigSQLiteDBHelper.CONFIG_COLUMN_ID + "= ?", new String[]{"1"}
        );

        if (rows < 1) {
            long newRowId = database.insert(ConfigSQLiteDBHelper.CONFIG_TABLE_NAME, null, values);
        }

        Toast.makeText(this, "Configuration saved", Toast.LENGTH_LONG).show();
    }

    private void readFromDB() {
        SQLiteDatabase database = new ConfigSQLiteDBHelper(this).getReadableDatabase();

        Cursor cursor = database.query(
                ConfigSQLiteDBHelper.CONFIG_TABLE_NAME,
                new String[]{
                        ConfigSQLiteDBHelper.CONFIG_COLUMN_URI,
                        ConfigSQLiteDBHelper.CONFIG_COLUMN_SERVICE_NUMBER,
                        ConfigSQLiteDBHelper.CONFIG_COLUMN_SERVICE_CONDITION
                },
                ConfigSQLiteDBHelper.CONFIG_COLUMN_ID + "= ?",
                new String[]{"1"},
                null,
                null,
                null
        );

        if (cursor.moveToFirst()) {
            do {
                String webhookURI = cursor.getString(
                        cursor.getColumnIndex(ConfigSQLiteDBHelper.CONFIG_COLUMN_URI));
                textWebhookURI.setText(webhookURI);

                String serviceNumber = cursor.getString(
                        cursor.getColumnIndex(ConfigSQLiteDBHelper.CONFIG_COLUMN_SERVICE_NUMBER));
                textServiceNumber.setText(serviceNumber);

                String serviceCondition = cursor.getString(
                        cursor.getColumnIndex(ConfigSQLiteDBHelper.CONFIG_COLUMN_SERVICE_CONDITION));
                textServiceCondition.setText(serviceCondition);
            } while (cursor.moveToNext());
        }

        cursor.close();
    }
}
